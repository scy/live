#!/bin/sh
set -e

certdir='/var/lib/mumble-server'

# Copy the certificates.
for f in fullchain privkey; do
	cp "$RENEWED_LINEAGE/$f.pem" "$certdir"
	chown mumble-server: "$certdir/$f.pem"
done

# Ask Murmur to reload the certificates.
kill -SIGUSR1 "$(cat /var/run/mumble-server/mumble-server.pid)"
