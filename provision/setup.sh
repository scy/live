#!/bin/sh
set -e

DOMAIN='scy.name'
FQDN="$(hostname).$DOMAIN"

# Update base system.
apt update
apt upgrade -y

# Install my preferred tools and the required packages.
apt install -y \
	automake \
	build-essential \
	cargo \
	certbot \
	clang \
	cmake \
	git \
	libevent-dev \
	libmsgpack-dev \
	libncurses-dev \
	libnice-dev \
	libssh-dev \
	libssl-dev \
	libtool \
	mumble-server \
	nginx-light \
	npm \
	pkg-config \
	python3-certbot-nginx \
	ruby \
	rustc \
	sqlite3 \
	tmux \
	vim \
	zlib1g-dev \
	;

# TODO: Copy the Certbot hook script(s).

# Request TLS certificate.
certbot run -n --agree-tos --nginx -m "scy-letsencrypt@$DOMAIN" -d "$FQDN"

# Set up Mumble-Web.
useradd -c 'Mumble-Web and Proxy' -m -r -s /bin/bash -U webmumble
sudo -u webmumble sh -c '
	cd \
	&& git clone https://github.com/johni0702/mumble-web-proxy.git \
	&& cd mumble-web-proxy \
	&& cargo build --release'
sudo -u webmumble sh -c '
	cd \
	&& git clone https://github.com/johni0702/mumble-web.git \
	&& cd mumble-web \
	&& npm install'

# Set up the tmate server.
useradd -c 'tmate' -m -r -s /bin/bash -U tmate
sudo -u tmate sh -c '
	cd \
	&& git clone https://github.com/tmate-io/tmate-ssh-server.git \
	&& cd tmate-ssh-server \
	&& ./create_keys.sh > ../create_keys.log 2>&1 \
	&& ./autogen.sh \
	&& ./configure \
	&& make'
cp tmate-ssh-server /usr/local/sbin
printf 'Port 22022\n' > /etc/ssh/sshd_config.d/different-port.conf
service ssh reload

# Little helper script to display the Murmur superuser password.
printf "#!/bin/sh\\nprintf 'select msg from slog order by msgtime asc limit 1;\\n' | sqlite3 /var/lib/mumble-server/mumble-server.sqlite | cut -d \\\\' -f 4\\n" > /usr/local/bin/murmur-superuser-password
chmod 0755 /usr/local/bin/murmur-superuser-password
