# live.scy.name

For my [Advent of Code 2020](https://adventofcode.com/2020/) livestreams I’m trying to migrate away from big streaming services while at the same time focusing more on a text-based experience.

This means that I’ll code in a plain text console and allow people to watch in their own text consoles, too.
In this repository you’ll find setup scripts, configuration and probably some web content to go with that setup.

As all of this is currently a work in progress and quite time-critical (it’s November 27 at the time of writing), documentation is a bit lacking, but will hopefully improve.

Feel free to look around and ask questions on stream.

## Software Stack

* [Mumble](https://www.mumble.info/) for realtime audio.
* [mumble-web](https://github.com/Johni0702/mumble-web) to lower the barrier for people who prefer to use their browser.
* [tmate](https://tmate.io/) to share my terminal session with everyone, both via anonymous read-only SSH and via the browser.

Chat will take place in [#livestream:scy.name](https://matrix.to/#/#livestream:scy.name).
Participation requires a [Matrix](https://matrix.org/) account, but there will be a read-only display of the chat directly in the stream image, powered by [gomuks](https://github.com/tulir/gomuks).
